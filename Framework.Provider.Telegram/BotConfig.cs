﻿namespace SimpleBot.Framework.Provider.Telegram
{
    public class BotConfig
    {
        public string ApiKey { get; set; }
        public ProxyConfig Proxy { get; set; }
        public int MessageMaxLenght { get; set; } = 4096;
    }
}